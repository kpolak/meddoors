package com.ge.meddoors.web.importer;

import com.ge.meddoors.web.importer.complex.HierarchyGenerator;
import com.ge.meddoors.web.importer.modules.DSModule;
import com.ge.meddoors.web.importer.modules.VTPModule;
import com.ge.meddoors.web.model.DesignStatement;
import com.ge.meddoors.web.model.TestCase;
import com.ge.meddoors.web.model.TreeNodeEnabled;
import org.junit.Test;
import org.primefaces.model.TreeNode;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Testing hierarchy generation.
 */
public class HierarchyGeneratorTest {

    // @Test
    public void testReadWithCsvBeanReader_ds() throws Exception {
        URL resourceUrl = getClass().getResource("/" + "sc_ds_with_links.csv");
        Reader reader = getTestReader(resourceUrl);

        List<DesignStatement> statements = CsvReader.readWithCsvBeanReader(reader, true,
                new DSModule().getProcessors(), DSModule.beanMapper);

//        TreeNode treeNode = HierarchyGenerator.generateTreeNodes(statements);
//        System.out.println(treeNode.getChildCount());


        for (DesignStatement ds : statements) {
            Pattern headerPattern = Pattern.compile("((\\p{Digit}+(\\p{Punct}\\p{Digit}+)*)+[^.](\\p{Blank})[^-])((\\p{ASCII})+)");
            if (ds == null || ds.getNodeDescription() == null) {
                continue;
            }

            Matcher matcher = headerPattern.matcher(ds.getNodeDescription());
            if (matcher.matches()) {

                String chapter = matcher.group(1).trim();
                // 01 parse - ____chapter___.subchapter
                System.out.println(">> " + chapter);
            }
        }

        System.out.println("DSs: " + statements.size());
    }


    @Test
    public void testReadWithCsvBeanReader_vtp() throws Exception {
        URL resourceUrl = getClass().getResource("/" + "sc_vtp_with_links.csv");
        Reader reader = getTestReader(resourceUrl);

        List<TestCase> testCases = CsvReader.readWithCsvBeanReader(reader, true,
                new VTPModule().getProcessors(), VTPModule.beanMapper);


        for (TestCase tc : testCases) {
            Pattern headerPattern = Pattern.compile("(((\\p{Digit}{1,3})(\\p{Punct}\\p{Digit}+)*)(\\p{Blank})([^-%]))(\\p{ASCII}+)");
            if (tc == null || tc.getNodeDescription() == null) {
                continue;
            }

            Matcher matcher = headerPattern.matcher(tc.getNodeDescription());
            if (matcher.matches()) {

                String chapter = matcher.group(1).trim();
                String desc = matcher.group(6) + matcher.group(7);
                // 01 parse - ____chapter___.subchapter
                System.out.println(">> " + chapter + " :: " + desc);
            }
        }

        System.out.println("VTPs: " + testCases.size());
    }



    private Reader getTestReader(URL resourceUrl) {
        try {
            Path resourcePath = Paths.get(resourceUrl.toURI());
            System.out.println(resourcePath.toAbsolutePath());
            return Files.newBufferedReader(resourcePath, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}
