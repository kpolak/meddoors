package com.ge.meddoors.web.importer;

import com.ge.meddoors.web.importer.modules.DSModule;
import com.ge.meddoors.web.importer.modules.VTPModule;
import com.ge.meddoors.web.model.DesignStatement;
import com.ge.meddoors.web.model.TestCase;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * JUnit test for CsvReader class.
 */
public class CsvReaderTest {

    @Test
    public void testReadRecords() throws Exception {
    }

    @Test
    public void testReadWithCsvBeanReader_ds() throws Exception {
        URL resourceUrl = getClass().getResource("/" + "sc_ds_with_links.csv");
        Reader reader = getTestReader(resourceUrl);

        List<DesignStatement> statements = CsvReader.readWithCsvBeanReader(reader, true,
                new DSModule().getProcessors(), DSModule.beanMapper);

        for (DesignStatement ds : statements) {
            Pattern headerPattern = Pattern.compile("((\\p{Digit}+\\p{Punct}*)+(\\p{Blank}))((\\p{ASCII})+)");
            Matcher matcher = headerPattern.matcher(ds.getStatement());
            if (matcher.matches()) {

                System.out.println(matcher.group(1));  // 1  4
//                System.out.println(ds);
            }
        }

        System.out.println("DSs: " + statements.size());
    }


    @Test
    public void testReadWithCsvBeanReader_vtp() throws Exception {
        URL resourceUrl = getClass().
                getResource("/" + "sc_vtp_with_links.csv");

        Reader reader = getTestReader(resourceUrl);

        List<TestCase> testCases = CsvReader.readWithCsvBeanReader(
                reader, true, new VTPModule().getProcessors(), VTPModule.beanMapper);

//        for (TestCase vtp : testCases) {
//            System.out.println("\n===================");
//            System.out.println(vtp);
//        }
        System.out.println("VTPs: " + testCases.size());
    }

    // /Users/kpolak/Downloads/doors_pack/SC_DS_with_links.CSV

    private Reader getTestReader(URL resourceUrl) {
        try {
            Path resourcePath = Paths.get(resourceUrl.toURI());
            System.out.println(resourcePath.toAbsolutePath());
            return Files.newBufferedReader(resourcePath, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}