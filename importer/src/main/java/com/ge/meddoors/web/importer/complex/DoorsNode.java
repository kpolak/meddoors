package com.ge.meddoors.web.importer.complex;

import lombok.Getter;

/**
 * Doors node representing single node in the table of contents tree.
 */
public class DoorsNode {

    private String name;

    @Getter
    private String nodeIndex;

    public DoorsNode(String name, String nodeIndex) {
        this.name = name;
        this.nodeIndex = nodeIndex;
    }

    @Override
    public String toString() {
        return name;
    }
}
