package com.ge.meddoors.web.importer.modules;

import com.ge.meddoors.web.importer.adaptors.MultiValuesAdaptor;
import com.ge.meddoors.web.model.DesignStatement;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.List;
import java.util.function.Function;

/**
 * Design Statements CSV specifics.
 */
public class DSModule implements ModuleCSVSpecifics {

    public static Function<List<Object>, DesignStatement> beanMapper = (vals) -> {
        DesignStatement ds = new DesignStatement();
        ds.setId(vals.get(0).toString());
        ds.setStatement(vals.get(1).toString());
        ds.setRisk(vals.get(2).toString());
        if (vals.get(3) != null) {
            ds.setRelease(vals.get(3).toString());
        }
        ds.setSdas((List<String>) vals.get(4));
        ds.setTestCases((List<String>) vals.get(5));
        ds.setLineNbr((int) vals.get(9));
        return ds;
    };

    public CellProcessor[] getProcessors() {

        return new CellProcessor[]{
                new Optional(), // id
                new Optional(), // statement
                new Optional(), // risk
                new Optional(), // release
                new Optional(new MultiValuesAdaptor()), // sdas
                new Optional(new MultiValuesAdaptor()), // test cases
                new Optional(), // n/a
                new Optional(), // n/a
                new Optional()  // n/a
        };
    }


}
