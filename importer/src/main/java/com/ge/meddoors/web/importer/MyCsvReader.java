package com.ge.meddoors.web.importer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * CSV files reader.
 * Simple implementation demonstrating reading file line by line.
 */
public class MyCsvReader {

    private static final String SEPARATOR = ",";
    public static Function<String, List<String>> parseLine = (line) -> {
        StringTokenizer stringTokenizer = new StringTokenizer(line, ",");
        while (stringTokenizer.hasMoreElements()) {
            System.out.println("ST: " + stringTokenizer.nextElement());
        }
        String[] p = line.split(",");
        return Arrays.asList(p);
    };
    private Reader source = null;


    public MyCsvReader(Reader source) {
        this.source = source;
    }

    public List<List<String>> readRecords(int linesToSkip) {
        try (BufferedReader reader = new BufferedReader(source)) {
            return reader.lines()
                    .skip(linesToSkip)
                    .map(parseLine)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}