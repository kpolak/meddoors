package com.ge.meddoors.web.importer.modules;

import com.ge.meddoors.web.importer.adaptors.MultiValuesAdaptor;
import com.ge.meddoors.web.model.TestCase;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.List;
import java.util.function.Function;

/**
 * DVTPs CSV specifics.
 */
public class VTPModule {
    public static Function<List<Object>, TestCase> beanMapper = (vals) -> {
        TestCase tc = new TestCase();
        tc.setId(vals.get(0).toString());
        if (vals.get(1) != null) {
            tc.setDescription(vals.get(1).toString());
        }
        if (vals.get(2) != null) {
            tc.setPrecondition(vals.get(2).toString());
        }
        if (vals.get(3) != null) {
            tc.setTestInput(vals.get(3).toString());
        }
        if (vals.get(4) != null) {
            tc.setTestOutput(vals.get(4).toString());
        }
        if (vals.get(5) != null) {
            tc.setResult(vals.get(5).toString());
        }
        if (vals.get(6) != null) {
            tc.setReviewPhases(vals.get(6).toString());
        }
        if (vals.get(7) != null) {
            tc.setComments(vals.get(7).toString());
        }
        if (vals.get(8) != null) {
//            System.out.println("8: " + vals.get(8));
        }
        if (vals.get(9) != null) {
//            System.out.println("9: " + vals.get(9));
        }
        if (vals.get(10) != null) {
//            System.out.println("10: " + vals.get(10));
        }
        if (vals.get(11) != null) {
//            System.out.println("11: " + vals.get(11));
        }
        tc.setSrs((List<String>) vals.get(12));
        if (vals.get(13) != null) {
//            System.out.println("13: " + vals.get(13));
        }
        if (vals.get(14) != null) {
//            System.out.println("14: " + vals.get(14));
        }
        if (vals.get(15) != null) {
//            System.out.println("15: " + vals.get(15));
        }

        tc.setLineNbr((int) vals.get(16));
        return tc;
    };

    public CellProcessor[] getProcessors() {

        return new CellProcessor[]{
                new Optional(), // 0 id
                new Optional(), // 1 description
                new Optional(), // 2 condition
                new Optional(), // 3 input
                new Optional(), // 4 output
                new Optional(), // 5 result
                new Optional(), // 6 review phases
                new Optional(), // 7 comments
                new Optional(), // 8 ddts
                new Optional(), // 9 standart
                new Optional(), // 10 outlinks1
                new Optional(), // 11 outlinks2
                new Optional(new MultiValuesAdaptor()), // 12 outlinks deph 1
                new Optional(), // 13 outlinks deph 2
                new Optional(), // 14 outlinks deph 3
                new Optional(), // 15 outlinks deph 4
//                new Optional(new MultiValuesAdaptor()), // sdas
//                new Optional(new MultiValuesAdaptor()), // test cases
        };
    }
}
