package com.ge.meddoors.web.importer.modules;

import com.ge.meddoors.web.model.DesignStatement;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.List;
import java.util.function.Function;

/**
 * Created by kpolak on 01/05/15.
 */
public interface ModuleCSVSpecifics {

    public CellProcessor[] getProcessors();

    //public Function<List<Object>, DesignStatement> parseDS();
}
