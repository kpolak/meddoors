package com.ge.meddoors.web.importer.adaptors;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.util.CsvContext;

import java.util.ArrayList;
import java.util.List;


public class MultiValuesAdaptor extends CellProcessorAdaptor {

    public MultiValuesAdaptor() {
        super();
    }

    public Object execute(Object value, CsvContext context) {

        validateInputNotNull(value, context);  // throws an Exception if the input is null

        List<String> valuesList = new ArrayList<>();

        String[] entries = value.toString().split("\n");
        for (String entry : entries) {
            if (entry != null && entry.trim().length() > 0) {
                valuesList.add(entry);
            }
        }

        return next.execute(valuesList, context);
    }
}
