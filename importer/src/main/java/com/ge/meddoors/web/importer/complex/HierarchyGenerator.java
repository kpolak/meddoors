package com.ge.meddoors.web.importer.complex;

import com.ge.meddoors.web.model.DesignStatement;
import com.ge.meddoors.web.model.TreeNodeEnabled;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Generates tree structure.
 */
public class HierarchyGenerator {

    public static TreeNode generateTreeNodes(List<? extends TreeNodeEnabled> elements) {

        Map<String, TreeNode> treeNodes = new HashMap<>();
        TreeNode root = new DefaultTreeNode(new DoorsNode("All Elements", ""), null);
        treeNodes.put("", root);


        String currentIndex = null;

        for (TreeNodeEnabled element : elements) {
            Pattern headerPattern = Pattern.compile("(((\\p{Digit}{1,3})(\\p{Punct}\\p{Digit}+)*)(\\p{Blank})([^-%]))(\\p{ASCII}+)");
            if (element == null || element.getNodeDescription() == null) {
                continue;
            }

            Matcher matcher = headerPattern.matcher(element.getNodeDescription());
            if (matcher.matches()) {

                String chapter = matcher.group(2).trim();
                // 01 parse - ____chapter___.subchapter
                String parentChapter = "";
                if (chapter.contains(".")) {
                    parentChapter = chapter.substring(0, chapter.lastIndexOf("."));
                    parentChapter = parentChapter.trim();
                }

                // 02 find parent
                TreeNode parentNode = treeNodes.get(parentChapter);

                if (parentNode == null) {
                    throw new RuntimeException("Didn't find parent node for: " + parentChapter + ", chapter: " + chapter);
                }

                // 03 add subchapter to parent
                TreeNode newNode = new DefaultTreeNode(new DoorsNode(element.getNodeDescription(), chapter));
                treeNodes.put(chapter, newNode);

                // 04 set current index
                currentIndex = chapter;

                parentNode.getChildren().add(newNode);
            } else {
                // set parent index
                if (currentIndex == null) {
                    throw new RuntimeException("DS starting without parent!");
                }
                element.setParentNodeIndex(currentIndex);
            }
        }

        return root;
    }

}
