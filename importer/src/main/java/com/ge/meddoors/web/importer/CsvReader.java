package com.ge.meddoors.web.importer;

import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Reads CSV file and provides objects created upon csv file.
 */
public class CsvReader {

    /**
     * @param reader     reader providing source data
     * @param skipHeader true, if first row should be skipped
     * @param mapping    mapping function to convert plain data to the output bean format
     * @return list of beans of type R constructed from provided cvs file
     * @throws Exception unexpected exception during reading file, mapping etc.
     */
    public static <R> List<R> readWithCsvBeanReader(Reader reader, boolean skipHeader,
                                                    CellProcessor[] processors, Function<List<Object>, R> mapping) throws Exception {
        try (ICsvListReader listReader = new CsvListReader(reader, CsvPreference.STANDARD_PREFERENCE)) {

            listReader.getHeader(skipHeader);

            final List<R> returnList = new ArrayList<>();
            List<Object> entries;
            while ((entries = listReader.read(processors)) != null) {
                entries.add(skipHeader ? listReader.getRowNumber() - 1 : listReader.getRowNumber());
                returnList.add(mapping.apply(entries));
            }
            return returnList;
        }
    }
}
