package com.ge.meddoors.web.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import java.util.List;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 26/11/13
 * Time: 13:57
 */
public class DBTest {

    public static void main(String[] args) {
        DBTest dbTest = new DBTest();
        dbTest.testDB();
    }

    private void testDB() {
/*
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("medtestPU");
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    TestVersion round1 = new TestVersion();
    round1.setVersionName("Version 01");

    TestVersion round1a = new TestVersion();
    round1a.setVersionName("Version 01-a");
    round1a.setParentTestVersion(round1);

    tx.begin();
    em.persist(round1);
    em.persist(round1a);

    Protocol p = new Protocol();
    p.setFileName("fileName");
    p.setTester("Krzysztof Polak");
    p.setTestVersion(round1a);

    ProtocolTestCase ptc = new ProtocolTestCase();
    ptc.setSprFlag(false);
//    ptc.setTestCase("TC_001");
    ptc.setExecutionDate(new DateTime());
    ptc.setObservedResults("No interesting results");
    ptc.setTestResult(ProtocolTestCase.TestResultEnum.PASSED);
    ptc.setTester("Krzysztof Polak");
    ptc.getTestCase().setPrecondition("This is a precondition");

    p.addTestCase(ptc);

    ptc = new ProtocolTestCase();
    ptc.setSprFlag(true);
//    ptc.setTestCase("TC_001");
    ptc.setExecutionDate(new DateTime());
    ptc.setObservedResults("Some results");
    ptc.setTestResult(ProtocolTestCase.TestResultEnum.PASSED);
    ptc.setTester("Krzysztof Polak");
    ptc.getTestCase().setPrecondition("Precondition");
    ptc.setProblemDescription("none");

    p.addTestCase(ptc);

    em.persist(p);
    tx.commit();

    List<Protocol> protocols = em.createNamedQuery("findAllProtocols").getResultList();
    System.out.println("Protocols in db: " + protocols.size());
    for (Protocol protocol : protocols) {
      System.out.println("==========================");
      System.out.println("Protocol: " + protocol.getId() + ", " + protocol.getFileName());
      List<ProtocolTestCase> testCases = protocol.getTestCases();
      for (ProtocolTestCase protocolTestCase : testCases) {
        System.out.println("__________");
        System.out.println("Test case: " + protocolTestCase.toString());
      }
    }

    em.close();
    emf.close();
*/
    }
}
