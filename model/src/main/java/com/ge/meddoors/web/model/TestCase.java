package com.ge.meddoors.web.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Test case definition entity.
 * User: Krzysztof Polak
 * Date: 20/11/13
 * Time: 15:17
 */
//@Entity
//@Table(name = "TEST_CASES")
//@NamedQueries({
//    @NamedQuery(name = "TestCase.findByCode", query = "SELECT tc FROM TestCase tc WHERE tc.testCode = :testCode"),
//    @NamedQuery(name = "TestCase.findAll", query = "SELECT a FROM TestCase a")
//})
@ToString
public class TestCase implements TreeNodeEnabled {

    @Getter
    @Setter
    String parentVTPIndex;
    //  @Id
//  @Column(name = "TEST_CODE")
    @Getter
    @Setter
    private String id;
    //  @Column(name = "DESCRIPTION", length = 4000)
    @Getter
    @Setter
    private String description;
    //  @Column(name = "PRECONDITION", length = 4000)
    @Getter
    private String precondition;
    //  @Column(name = "TEST_INPUT", length = 4000)
    @Getter
    private String testInput;
    //  @Column(name = "TEST_OUTPUT", length = 4000)
    @Getter
    private String testOutput;
    //  @Column(name = "RESULT", length = 4000)
    @Getter
    @Setter
    private String result;
    //  @Column(name = "REVIEW_PHASES", length = 200)
    @Getter
    @Setter
    private String reviewPhases;
    //  @Column(name = "COMMENTS", length = 200)
    @Getter
    @Setter
    private String comments;
    private List<String> srs;
    @Getter
    @Setter
    private int lineNbr;
    @Getter
    @Setter
    private List<DesignStatement> designStatements;


    public TestCase() {
    }

    public TestCase(TestCase testCase) {
        this.precondition = testCase.getPrecondition();
        this.testInput = testCase.getTestInput();
        this.testOutput = testCase.getTestOutput();
        this.srs = testCase.getSrs();
        this.parentVTPIndex = testCase.getParentVTPIndex();
        this.lineNbr = testCase.getLineNbr();
        this.reviewPhases = testCase.getReviewPhases();
        this.description = testCase.getDescription();
        this.comments = testCase.getComments();
        this.id = testCase.getId();
        this.result = testCase.getResult();
        this.designStatements = new ArrayList<>();
        if (testCase.getDesignStatements() != null) {
            for (DesignStatement ds : testCase.getDesignStatements()) {
                DesignStatement newDs = new DesignStatement(ds);
                this.designStatements.add(newDs);
            }
        }
    }

    public void setPrecondition(String precondition) {
        if (precondition != null && precondition.length() > 4000) {
            this.precondition = precondition.substring(0, 3999);
        } else {
            this.precondition = precondition;
        }
    }

    public void setTestInput(String testInput) {
        if (testInput != null && testInput.length() > 4000) {
            this.testInput = testInput.substring(0, 3999);
        } else {
            this.testInput = testInput;
        }
    }

    public void setTestOutput(String testOutput) {
        if (testOutput != null && testOutput.length() > 4000) {
            this.testOutput = testOutput.substring(0, 3999);
        } else {
            this.testOutput = testOutput;
        }
    }

    public List<String> getSrs() {
        return srs;
    }

    public void setSrs(List<String> srs) {
        this.srs = srs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestCase testCase = (TestCase) o;

        if (lineNbr != testCase.lineNbr) return false;
        if (id != null ? !id.equals(testCase.id) : testCase.id != null) return false;
        if (description != null ? !description.equals(testCase.description) : testCase.description != null)
            return false;
        if (precondition != null ? !precondition.equals(testCase.precondition) : testCase.precondition != null)
            return false;
        if (testInput != null ? !testInput.equals(testCase.testInput) : testCase.testInput != null) return false;
        if (testOutput != null ? !testOutput.equals(testCase.testOutput) : testCase.testOutput != null) return false;
        if (result != null ? !result.equals(testCase.result) : testCase.result != null) return false;
        if (reviewPhases != null ? !reviewPhases.equals(testCase.reviewPhases) : testCase.reviewPhases != null)
            return false;
        return !(comments != null ? !comments.equals(testCase.comments) : testCase.comments != null) && !(srs != null ? !srs.equals(testCase.srs) : testCase.srs != null);

    }

    @Override
    public int hashCode() {
        int result1 = id != null ? id.hashCode() : 0;
        result1 = 31 * result1 + (description != null ? description.hashCode() : 0);
        result1 = 31 * result1 + (precondition != null ? precondition.hashCode() : 0);
        result1 = 31 * result1 + (testInput != null ? testInput.hashCode() : 0);
        result1 = 31 * result1 + (testOutput != null ? testOutput.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (reviewPhases != null ? reviewPhases.hashCode() : 0);
        result1 = 31 * result1 + (comments != null ? comments.hashCode() : 0);
        result1 = 31 * result1 + (srs != null ? srs.hashCode() : 0);
        result1 = 31 * result1 + lineNbr;
        return result1;
    }

    @Override
    public String getNodeDescription() {
        return description;
    }

    @Override
    public String getParentNodeIndex() {
        return parentVTPIndex;
    }

    @Override
    public void setParentNodeIndex(String parentIndex) {
        parentVTPIndex = parentIndex;
    }
}
