package com.ge.meddoors.web.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Config data entity - contains data for a single configuration
 * User: Krzysztof Polak
 * Date: 06/12/13
 * Time: 13:47
 */
@Entity
@Table(name = "DESIGN_STATEMENT")
@ToString
//@NamedQuery(name = "DesignStatement.findAll", query = "SELECT ds FROM DesignStatement ds")
public class DesignStatement implements TreeNodeEnabled {

    @Id
    @Getter
    @Setter
    String id;

    @Getter
    @Setter
    Integer lineNbr;

    @Getter
    @Setter
    String statement;

    @Getter
    @Setter
    String risk;

    @Getter
    @Setter
    String release;

    @Getter
    @Setter
//  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
            List<String> sdas = new ArrayList<>();

    // mappedBy = "protocol"
    @Getter
    @Setter
//  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
            List<String> testCases = new ArrayList<>();

//    @Getter
//    @Setter
//    List<TestCase> testCaseList = new ArrayList<>();

    @Getter
    @Setter
    List<TestCase> sdasList = new ArrayList<>();

    @Getter
    @Setter
    String parentDSIndex;

    public DesignStatement() {
    }

    public DesignStatement(DesignStatement ds) {
        this.id = ds.getId();
        this.lineNbr = ds.getLineNbr();
        this.statement = ds.getStatement();
        this.risk = ds.getRisk();
        this.release = ds.getRelease();
        this.parentDSIndex = ds.getParentDSIndex();
        this.testCases = new ArrayList<>();
        if (ds.getTestCases() != null) {
            this.testCases.addAll(ds.getTestCases());
        }
    }


    @Override
    public String getNodeDescription() {
        return statement;
    }

    @Override
    public String getParentNodeIndex() {
        return parentDSIndex;
    }

    @Override
    public void setParentNodeIndex(String parentIndex) {
        parentDSIndex = parentIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DesignStatement that = (DesignStatement) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (lineNbr != null ? !lineNbr.equals(that.lineNbr) : that.lineNbr != null) return false;
        if (statement != null ? !statement.equals(that.statement) : that.statement != null) return false;
        if (risk != null ? !risk.equals(that.risk) : that.risk != null) return false;
        if (release != null ? !release.equals(that.release) : that.release != null) return false;
        if (sdas != null ? !sdas.equals(that.sdas) : that.sdas != null) return false;
        return !(testCases != null ? !testCases.equals(that.testCases) : that.testCases != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (lineNbr != null ? lineNbr.hashCode() : 0);
        result = 31 * result + (statement != null ? statement.hashCode() : 0);
        result = 31 * result + (risk != null ? risk.hashCode() : 0);
        result = 31 * result + (release != null ? release.hashCode() : 0);
        result = 31 * result + (sdas != null ? sdas.hashCode() : 0);
        result = 31 * result + (testCases != null ? testCases.hashCode() : 0);
        return result;
    }

}
