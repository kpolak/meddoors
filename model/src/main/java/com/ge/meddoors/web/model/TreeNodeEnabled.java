package com.ge.meddoors.web.model;

/**
 * This interface provides method needed for displaying object in the Tree.
 */
public interface TreeNodeEnabled {

    String getNodeDescription();

    String getParentNodeIndex();

    void setParentNodeIndex(String parentIndex);


}
