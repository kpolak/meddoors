package com.ge.meddoors.web.model.utils;

//import org.joda.time.DateTime;
//import org.joda.time.format.DateTimeFormat;
//import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: kpolak
 * Date: 01/12/13
 * Time: 21:55
 * To change this template use File | Settings | File Templates.
 */
public class StringUtils {

    // "30-Okt-2013 13:20:45"
    public static final String DATE_PARSE_PATTERN = "dd-MMM-yyyy";
    public static final String GLOBAL_DATE_TIME_FORMAT = "yyyy-MM-dd kk:mm:ss";
    public static final String GLOBAL_DATE_FORMAT = "yyyy-MM-dd";
//  private static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(StringUtils.GLOBAL_DATE_FORMAT);
//  private static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(StringUtils.GLOBAL_DATE_TIME_FORMAT);

    public static String nvl(final String s, final Object o) {
        if (o != null && o.toString().length() > 0) {
            return "\n*" + s + ": '" + o.toString().replaceAll("\n", "\n\t") + '\'';
        }
        return "";
    }

    public static boolean isEmpty(String value) {
        return value == null || value.length() == 0;
    }

    public static Date transformToDate(String dateString) throws ParseException {
        return new SimpleDateFormat(DATE_PARSE_PATTERN, Locale.GERMAN).parse(dateString);
    }

//  public static String formatDate(DateTime dt, boolean withTime) {
//    if (withTime) {
//      return dateTimeFormatter.print(dt);
//    } else {
//      return dateFormatter.print(dt);
//    }
//  }
}
