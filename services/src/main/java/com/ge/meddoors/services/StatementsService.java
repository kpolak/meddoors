package com.ge.meddoors.services;

import com.ge.meddoors.web.importer.CsvReader;
import com.ge.meddoors.web.importer.complex.HierarchyGenerator;
import com.ge.meddoors.web.importer.modules.DSModule;
import com.ge.meddoors.web.model.DesignStatement;
import org.primefaces.model.TreeNode;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Named;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Statements service.
 */

@Named
@Singleton
public class StatementsService {

    private static List<DesignStatement> allStatements = null;
    private static List<DesignStatement> statements = null;

    private TreeNode dsRoot = null;

    @PostConstruct
    private void loadData() {
        Reader reader = getTestReader();

        try {
            allStatements = CsvReader.readWithCsvBeanReader(reader, true,
                    new DSModule().getProcessors(), DSModule.beanMapper);

            statements = new ArrayList<>();
            statements.addAll(allStatements);

            dsRoot = HierarchyGenerator.generateTreeNodes(statements);

        } catch (Exception e) {
            throw new RuntimeException(e.getCause());
        }
    }

    public List<DesignStatement> getStatements() {
        if (statements == null) {
            loadData();
        }
        return statements;
    }

    private Reader getTestReader() {
        try {
//            Path resourcePath = Paths.get("/Users/kpolak/Downloads/doors_pack/SC_DS_with_links.CSV");
            Path resourcePath = Paths.get("d:\\src\\myApps\\meddoors\\importer\\src\\test\\resources\\sc_ds_with_links.csv");
            System.out.println(resourcePath.toAbsolutePath());
            return Files.newBufferedReader(resourcePath, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Optional<DesignStatement> getDesignStatement(String dsId) {
        return statements.stream().filter(ds -> ds.getId().equals(dsId)).findFirst();
    }

    public TreeNode getDsRoot() {
        if (dsRoot == null) {
            loadData();
        }
        return dsRoot;
    }

    public List<DesignStatement> filterDS(String nodeIndex) {
        List filtered = new ArrayList<>();
        for (DesignStatement ds : allStatements) {
            if (ds.getParentDSIndex() != null && ds.getParentDSIndex().equals(nodeIndex)) {
                filtered.add(new DesignStatement(ds));
            }
        }

        statements = filtered;
        return statements;
    }

    public List<DesignStatement> getAllDesignStatements() {
        return allStatements;
    }
}
