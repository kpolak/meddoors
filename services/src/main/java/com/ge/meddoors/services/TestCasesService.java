package com.ge.meddoors.services;

import com.ge.meddoors.web.importer.CsvReader;
import com.ge.meddoors.web.importer.complex.HierarchyGenerator;
import com.ge.meddoors.web.importer.modules.VTPModule;
import com.ge.meddoors.web.model.TestCase;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Named;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Test cases service.
 */
@Named
@Singleton
public class TestCasesService {

    private static List<TestCase> allTestCases = null;
    private static List<TestCase> testCases = null;

    private TreeNode vtpRoot = null;

    @PostConstruct
    private void loadData() {
        Reader reader = getTestReader();

        try {
            allTestCases = CsvReader.readWithCsvBeanReader(reader, true,
                    new VTPModule().getProcessors(), VTPModule.beanMapper);
            testCases = new ArrayList<>();
            testCases.addAll(allTestCases);

            vtpRoot = HierarchyGenerator.generateTreeNodes(testCases);

        } catch (Exception e) {
            throw new RuntimeException(e.getCause());
        }

    }

    public List<TestCase> getTestCases() {
        if (testCases == null) {
            loadData();
        }
        return testCases;
    }

    private Reader getTestReader() {
        try {
//            Path resourcePath = Paths.get("/Users/kpolak/Downloads/doors_pack/SC_VTP_with_links.CSV");
            Path resourcePath = Paths.get("d:\\src\\myApps\\meddoors\\importer\\src\\test\\resources\\sc_vtp_with_links.csv");
            System.out.println(resourcePath.toAbsolutePath());
            return Files.newBufferedReader(resourcePath, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Optional<TestCase> getTestCase(String tcId) {
        return testCases.stream().filter(tc -> tc.getId().equals(tcId)).findFirst();
    }

    public TreeNode getVtpRoot() {
        if (vtpRoot == null) {
            loadData();
        }
        return vtpRoot;
    }

    public List<TestCase> filterVTP(String nodeIndex) {
        List filtered = new ArrayList<>();
        for (TestCase tc : allTestCases) {
            if (tc.getParentVTPIndex() != null && tc.getParentVTPIndex().equals(nodeIndex)) {
                filtered.add(new TestCase(tc));
            }
        }

        testCases = filtered;
        return testCases;
    }

    public List<TestCase> getAllTestCases() {
        return allTestCases;
    }
}
