package com.ge.meddoors.web.web.backbeans;

import com.ge.meddoors.services.StatementsService;
import com.ge.meddoors.services.TestCasesService;
import com.ge.meddoors.web.importer.complex.DoorsNode;
import com.ge.meddoors.web.model.DesignStatement;
import com.ge.meddoors.web.model.TestCase;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Back bean for VTPs tab.
 */
@ManagedBean(name = "testCasesView")
@ViewScoped
public class TestCasesView implements Serializable {

    @Inject
    private StatementsService statementsService;

    @Inject
    private TestCasesService testCasesService;

    private List<TestCase> testCases = null;

    @Setter
    private List<TestCase> filteredTestCases = null;

    @Getter
    @Setter
    private String tempDS;

    @Getter
    @Setter
    private TreeNode selectedNode;

    public List<TestCase> getFilteredTestCases() {
        if (filteredTestCases == null) {
            filteredTestCases = new ArrayList<>();
            filteredTestCases.addAll(getTestCases());
        }
        return filteredTestCases;
    }

    public List<TestCase> getTestCases() {
        if (testCases == null) {
            testCases = testCasesService.getTestCases();
        }
        return testCases;
    }

    public TreeNode getVtpRoot() {
        return testCasesService.getVtpRoot();
    }

    public DesignStatement getTempDSData() {
        return statementsService.getDesignStatement(tempDS).orElse(null);
    }

    public void displaySelected(ActionEvent event) {
        if (selectedNode != null) {
//            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", selectedNode.getData().toString());
//            FacesContext.getCurrentInstance().addMessage(null, message);

            DoorsNode selectedDoorsNode = (DoorsNode) selectedNode.getData();

            testCases = testCasesService.filterVTP(selectedDoorsNode.getNodeIndex());
            filteredTestCases = testCases;

            RequestContext.getCurrentInstance().update("schedulerTabView:schedulerVTPForm:vtpTab");

            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("schedulerTabView:schedulerVTPForm:vtpTab");
            System.out.println("Selected " + selectedNode.getData().toString());
        }
    }

    public void displayAll(ActionEvent event) {
        testCases = testCasesService.getAllTestCases();
        filteredTestCases = testCases;
        RequestContext.getCurrentInstance().update("schedulerTabView:schedulerVTPForm:vtpTab");
    }
}
