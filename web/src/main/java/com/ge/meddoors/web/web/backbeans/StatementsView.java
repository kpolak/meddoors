package com.ge.meddoors.web.web.backbeans;

import com.ge.meddoors.services.StatementsService;
import com.ge.meddoors.services.TestCasesService;
import com.ge.meddoors.web.importer.complex.DoorsNode;
import com.ge.meddoors.web.model.DesignStatement;
import com.ge.meddoors.web.model.TestCase;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Statements back bean.
 */
@ManagedBean(name = "statementsView")
@ViewScoped
public class StatementsView {

    @Inject
    private StatementsService statementsService;

    @Inject
    private TestCasesService testCasesService;

    private List<DesignStatement> designStatements = null;

    @Setter
    private List<DesignStatement> filteredStatements = null;

    @Getter
    @Setter
    private String tempTC;

    @Getter
    @Setter
    private TreeNode selectedNode;

    public List<DesignStatement> getFilteredStatements() {
        if (filteredStatements == null) {
            filteredStatements = new ArrayList<>();
            filteredStatements.addAll(getDesignStatements());
        }
        return filteredStatements;
    }

    public List<DesignStatement> getDesignStatements() {
        if (designStatements == null) {
            designStatements = statementsService.getStatements();
        }
        return designStatements;
    }

    public TreeNode getDsRoot() {
        return statementsService.getDsRoot();
    }


    public TestCase getTempTCData() {
        return testCasesService.getTestCase(tempTC).orElse(null);
    }

    public void displaySelected(ActionEvent event) {
        if (selectedNode != null) {
//            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", selectedNode.getData().toString());
//            FacesContext.getCurrentInstance().addMessage(null, message);

            DoorsNode selectedDoorsNode = (DoorsNode) selectedNode.getData();

            designStatements = statementsService.filterDS(selectedDoorsNode.getNodeIndex());
            filteredStatements = designStatements;

            RequestContext.getCurrentInstance().update("schedulerTabView:schedulerDSForm:dsTab");
            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("schedulerTabView:schedulerDSForm:dsTab");

            System.out.println("Selected " + selectedNode.getData().toString());
        }
    }

    public void displayAll(ActionEvent event) {
        designStatements = statementsService.getAllDesignStatements();
        filteredStatements = designStatements;
        RequestContext.getCurrentInstance().update("schedulerTabView:schedulerDSForm:dsTab");
    }
}